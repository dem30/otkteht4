import unittest
import lista_apuri

class Test_TestaaListaApuri(unittest.TestCase):
    def testaa_parillisten_maara(self):
        # Parillisia ja parittomia 5 kpl
        lista = [1,2,3,4,5,6,7,8,9,10]
        tulos = lista_apuri.parillisten_maara(lista)
        self.assertEqual(tulos, 5)

    def testaa_parittomien_maara(self):
        # Parillisia ja parittomia 5 kpl
        lista = [1,2,3,4,5,6,7,8,9,10]
        tulos = lista_apuri.parittomien_maara(lista)
        self.assertEqual(tulos, 5)

    def testaa_negatiivisten_maara(self):
        # Parillisia ja parittomia 5 kpl, negatiivisia 5 kpl
        lista = [1,-2,-3,4,-5,-6,7,8,9,-10]
        tulos = lista_apuri.negatiivisten_maara(lista)
        self.assertEqual(tulos, 5)